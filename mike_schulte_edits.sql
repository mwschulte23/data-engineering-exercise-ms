with cte as (
    select 'visitors' as aggregation
         , e.transaction_type
         , convert(e.visit_start_timestamp, date) as transaction_date
         , count(distinct e.visitor_id) as total
         , count(distinct case when e.transaction_action = 'transaction_started' then e.visitor_id end) as started
         , count(distinct case when e.transaction_action = 'transaction_failed' then e.visitor_id end) as failed
         , count(distinct case when e.transaction_action = 'transaction_completed' then e.visitor_id end) as completed
      from evolytics.exercise e
    group by 1, 2, 3
    
    UNION
    
    select 'visits' as aggregation
         , e.transaction_type
         , convert(e.visit_start_timestamp, date) as transaction_date
         , count(distinct concat(e.visitor_id, '|', e.visit_num)) as total
         , count(distinct case when e.transaction_action = 'transaction_started' then concat(e.visitor_id, '|', e.visit_num) end) as started
         , count(distinct case when e.transaction_action = 'transaction_failed' then concat(e.visitor_id, '|', e.visit_num) end) as failed
         , count(distinct case when e.transaction_action = 'transaction_completed' then concat(e.visitor_id, '|', e.visit_num) end) as completed
      from evolytics.exercise e
    group by 1, 2, 3
	)

select c.aggregation
     , 'all' transaction_type
     , c.transaction_date
     , sum(c.total) total
     , sum(c.started) started
     , sum(c.failed) failed
     , sum(c.completed) completed
  from cte c
group by 1, 2, 3

UNION

select c.aggregation
     , c.transaction_type
     , c.transaction_date
     , c.total
     , c.started
     , c.failed
     , c.completed
  from cte c
;